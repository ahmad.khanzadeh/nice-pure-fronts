const navMenu=document.getElementById('nav-menu'),
      navToggle=document.getElementById('nav-toggle')
      navClose=document.getElementById('nav-close')

/* 
=====================================================================
show & Hide menu section
*/
if(navToggle){
    navToggle.addEventListener('click',()=>{
        navMenu.classList.add('show-menu')
    })
}

if(navClose){
    navClose.addEventListener('click',()=>{
       navMenu.classList.remove('show-menu') 
    })
}
/*
=====================================================================
remove mobile menu
*/
const navLink=document.querySelectorAll('.nav__link')

function linkAction(){
    const navmenu= document.getElementById('nav-menu');
    // menu will be disapers just after clicking any link
    navMenu.classList.remove('show-menu')
}
navLink.forEach(singleLink =>singleLink.addEventListener('click',linkAction))
/* 
======================================================================
change background header
*/
function scrollHeader(){
    const header=document.getElementById('header')
    // display nav just after user scroll down more then 200px
    if(this.sceollY >=100){
        header.classList.add('scroll-header');
    }else{
        header.classList.remove('scroll-header');
    }
}
window.addEventListener('scroll', scrollHeader);
/* 
======================================================================
swiper section
*/
// let swiper = new Swiper(".discover__container", {
//     effect: "coverflow",
//     grabCursor: true,
//     centeredSlides: true,
//     slidesPerView: "auto",
//     loop: true,
//     spaceBetween: 32,
//     coverflowEffect: {
//         rotate: 0,
//     },
// })

var swiper = new Swiper(".mySwiper", {
    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    slidesPerView: "auto",
    loop:true,
    spaceBetween:32,
    coverflowEffect: {
      rotate: 10,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    },
    pagination: {
      el: ".swiper-pagination",
    },
  });
/* 
========================================================
video play section
*/
const videoFile=document.getElementById('video-file'),
        vidoeButton=document.getElementById('video-button'),
        videoIcon=document.getElementById('video-icon')

function playPause(){
    if(videoFile.paused){
        // play the video
        videoFile.play()
        // changing button icon during play
        videoIcon.classList.add('ri-pause-line')
        videoIcon.classList.remove('ri-play-line')
    }else{
        // pause video
        videoFile.pause()
        // change the icon 
        videoIcon.classList.remove('ri-pause-line')
        videoIcon.classList.add('ri-play-line')
        
    }
}
vidoeButton.addEventListener('click',playPause)

function finalVideo(){
    // change icon when video ends
    videoIcon.classList.remove('ri-pause-line') 
    videoIcon.classList.add('ri-play-line') 
}
// call finalVidoe function soon as 
videoFile.addEventListener('ended', finalVideo)

// scrollup function ( click to go up )
function scrollUp(){
    const scrollUp=document.getElementById('scroll-up');
    if(this.scrollY >=210){
        scrollUp.classList.add('show-scroll');
    }else{
        scrollUp.classList.remove('show-scroll');
    }
    window.addEventListener('scroll', scrollUp)
}
// to activate every single link on top
const sections = document.querySelectorAll('section[id]')

function scrollActive(){
    const scrollY = window.pageYOffset

    sections.forEach(current =>{
        const sectionHeight = current.offsetHeight
        const sectionTop = current.offsetTop - 50;
        const sectionId = current.getAttribute('id')

        if (scrollY > sectionTop && scrollY <= sectionTop + sectionHeight){
            document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.add('active-link')
        } else{
            document.querySelector('.nav__menu a[href*=' + sectionId + ']').classList.remove('active-link')
        }
    })
}

window.addEventListener('scroll', scrollActive)
// dark mode functionality
const themeButton = document.getElementById('theme-button')
const darkTheme = 'dark-theme'
const iconTheme = 'ri-sun-line'

// Previously selected topic (if user selected)
const selectedTheme = localStorage.getItem('selected-theme')
const selectedIcon = localStorage.getItem('selected-icon')

// We obtain the current theme that the interface has by validating the dark-theme class
const getCurrentTheme = () => document.body.classList.contains(darkTheme) ? 'dark' : 'light'
const getCurrentIcon = () => themeButton.classList.contains(iconTheme) ? 'ri-moon-line' : 'ri-sun-line'

// We validate if the user previously chose a topic
if (selectedTheme) {
  // If the validation is fulfilled, we ask what the issue was to know if we activated or deactivated the dark
  document.body.classList[selectedTheme === 'dark' ? 'add' : 'remove'](darkTheme)
  themeButton.classList[selectedIcon === 'ri-moon-line' ? 'add' : 'remove'](iconTheme)
}

// Activate / deactivate the theme manually with the button
themeButton.addEventListener('click', () => {
    // Add or remove the dark / icon theme
    document.body.classList.toggle(darkTheme)
    themeButton.classList.toggle(iconTheme)
    // We save the theme and the current icon that the user chose
    localStorage.setItem('selected-theme', getCurrentTheme())
    localStorage.setItem('selected-icon', getCurrentIcon())
})
// ===================================================
// =====scroll reveal animation
// ===================================================
const sr= ScrollReveal({
    distance: '60px',
    duration: 2800,
    rest: true
})
sr.ScrollReveal(`.home__data, .home__social-link, .home__info, .discover__container, .experience__data, .experience__overlay, .place__card, .sponser__content, .footer__rights`,{
    origin: 'top',
    interval: 100,
})
sr.ScrollReveal(`.about__data, .video__description, .subscribe__description`,{
    origin: 'left',
})
sr.ScrollReveal(`.about__img-overlay,.video__content,.subscribe__form`,{
    origin: 'right',
    interval:100,
})
