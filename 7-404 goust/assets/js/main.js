// show the menu
const navMenu=document.getElementById('nav-menu'),
      navToggle=document.getElementById('nav-toggle'),
      navClose=document.getElementById('nav-close');

    //   display the mobile menu by adding an ad event listener
if(navToggle){
    navToggle.addEventListener('click',()=>{ navMenu.classList.add('show-menu')})
}

    // hide the mobile menu
if(navClose){
    navClose.addEventListener('click',()=>{navMenu.classList.remove('show-menu')})
}
// remove mobile menu
const navLink = document.querySelectorAll('.nav__link')

function linkAction(){
    const navMenu=document.getElementById('nav-menu')
    // when each nav link is clicked, 'show-menu' will removed from class lists
    navMenu.classList.remove('show-menu')
}

navLink.forEach(singleNavElement => singleNavElement.addEventListener('click', linkAction))

// scrollreveal section
const revealSection= ScrollReveal({
    distance:'90px',
    duration:3000
})

revealSection.reveal(`.home__data`,{origin:'top', delay:400});
revealSection.reveal(`.home__image`,{origin:'bottom', delay:600});
revealSection.reveal(`.home__footer`,{origin:'bottom', delay:600});
