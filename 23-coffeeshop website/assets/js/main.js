// loader
onload = () =>{
    const load = document.getElementById('load')
    setTimeout(()=>{
        load.style.display='none'
    },2500)
}
// shwo menu in mobile 
const navMenu=document.getElementById('nav-menu'),
        navToggle=document.getElementById('nav-toggle'),
        navClose=document.getElementById('nav-close')
 


if(navToggle){
    navToggle.addEventListener('click', ()=>{
        navMenu.classList.add('show-menu')
    })
}
if(navClose){
    navClose.addEventListener('click', ()=>{
        navMenu.classList.remove('show-menu')
    })
}

// remove menu Mobile
const navLink=document.querySelectorAll('.nav__link')

function linkAction(){
    const navMenu=document.getElementById('nav-menu')
    //when use click on each nav__link, the show menu will be removed
    navMenu.classList.remove('show-menu')
}

navLink.forEach(n => n.addEventListener('click', linkAction))

// change background header
function scrollHeader(){
    const header=document.getElementById('header')
    // if user scroll more then 50% of viewport, add the stickey class to header
    if(this.scrollY <=50){
        header.classList.add('scroll-header');
    }else{
        header.classList.remove('scroll-header')
    }
}

window.addEventListener('scroll', scrollHeader)

// mixitup filter products 
    //    when the product is selected from the ul, the cards will be sorted again so only the cards which are 
    // related to the selected item will be displayed in the main page
let mixerProducts =mixitup('.products__content',{
    selectors:{
        target:'.products__card'
    },
    animation:{
        duration: 300
    }
});

// Default Filter products
mixerProducts.filter('.delicacies')
// link active Products
const linkProducts=document.querySelectorAll('.products__item')

function activeProducts(){
    linkProducts.forEach(l=>l.classList.remove('active-product'))
    this.classList.add('active-product')
}

linkProducts.forEach(l=>l.addEventListener('click', activeProducts))


// shwo scroll up 
function scrollUp(){
    const scrollUp =document.getElementById('scroll-up');
    // when the scroll is higher than 350 viewport height, add the showscroll class to the 
    if(this.scrollY >=350) scrollUp.classList.add('show-scroll'); else scrollUp.classList.remove('show-scroll')
}

window.addEventListener('scroll', scrollUp);

// scroll section active link

const sections=document.querySelectorAll('section[id]')

function scrollActive(){
    const scrollY=window.pageYOffset

    section.forEach(current =>{
        const sectionHeight = current.offsetHeight,
              sectionTop=current.offsetTop -58,
              sectionId=current.getAttribute('id')

       if(scrollY >sectionTop && scrollY <= sectionTop + sectionHeight){
        document.querySelector('.nav__menu a[href*='+sectionId +']').classList.add('active-link')
       } else{
        document.querySelector('.nav__menu a[href*='+sectionId+']').classList.remove('active-link')
       }
    })
}
window.addEventListener('scroll',scrollActive)